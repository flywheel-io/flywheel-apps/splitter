from unittest.mock import MagicMock

import pandas as pd

from fw_gear_dicom_splitter.dicom_splitter.base import (
    MultiSplitter,
    SingleSplitter,
    Splitter,
)


def test_init_splitter(dcmcoll, make_concrete):
    coll = dcmcoll(**{"test1": {}, "test2": {}})
    make_concrete(Splitter)
    splitter = Splitter(coll, 5)

    assert splitter.files == coll
    assert splitter.decision_val == 5


def test_get_neighbor_dist(dcmcoll, make_concrete):
    coll = dcmcoll(**{"test1": {}, "test2": {}, "test3": {}})
    make_concrete(Splitter)

    def dist(a, b):
        return 1

    df = pd.DataFrame.from_records([{"path": dcm.filepath} for dcm in coll])

    splitter = Splitter(coll, 5)
    splitter.get_neighbor_dist(df, dist)

    # Return 1 for first two, and 0 for last
    assert list(df["value"].values) == [1, 1, 0]


def test_neighbor_decision(dcmcoll, make_concrete):
    coll = dcmcoll(**{"test1": {}, "test2": {}, "test3": {}})
    make_concrete(Splitter)
    splitter = Splitter(coll, 5)
    df = pd.DataFrame.from_records(
        [
            {"path": dcm.filepath, "value": p, "p": p}
            for p, dcm in zip([0.01, 0.1, 0.5], coll)
        ]
    )
    thresh = 0.1
    out = splitter.neighbor_decision(df, thresh)
    assert all(col not in out.columns for col in ["value", "p", "split_group"])
    assert list(df["decision"].values) == [5.0, 0.0, 0.0]


def test_single_splitter(make_concrete, dcmcoll, mocker):
    make_concrete(SingleSplitter)
    coll = dcmcoll(**{"test1": {}, "test2": {}, "test3": {}})
    splitter = SingleSplitter(coll, 5)
    calc = mocker.patch.object(splitter, "calc_value")
    dec = mocker.patch.object(splitter, "decision")

    df = pd.DataFrame.from_records([{"path": dcm.filepath} for dcm in coll])
    out = splitter.split(df)
    calc.assert_called_once()
    dec.assert_called_once()
    assert isinstance(out, MagicMock)


def test_multi_splitter(make_concrete, dcmcoll, mocker):
    make_concrete(MultiSplitter)
    coll = dcmcoll(**{"test1": {}, "test2": {}, "test3": {}})
    splitter = MultiSplitter(coll, 5)
    calc = mocker.patch.object(splitter, "calc_value")
    dec = mocker.patch.object(splitter, "decision")
    df = pd.DataFrame.from_records([{"path": dcm.filepath} for dcm in coll])
    out = splitter.split(df)
    calc.assert_called_once()
    dec.assert_called_once()
    assert isinstance(out, tuple)
    assert not len(out)
