from unittest.mock import MagicMock

import numpy as np
import pandas as pd
import pytest
from pydicom import encaps, uid

from fw_gear_dicom_splitter.splitters import (
    EuclideanSplitter,
    JensenShannonDICOMSplitter,
    UniqueTagMultiSplitter,
    UniqueTagSingleSplitter,
    split_by_geometry,
)

from . import test_geometry


def test_euclidean_splitter_init(dcmcoll):
    # coll = dcmcoll(**{"test1": {}, "test2": {}})
    splitter = EuclideanSplitter(dcmcoll, 5, "test")
    assert splitter.tag == "test"


@pytest.fixture
def euclidean(dcmcoll):
    def _gen(iops):
        coll = dcmcoll(
            **{
                f"test{i}": {
                    "ImageOrientationPatient": ("DS", [0, 0, 0, 0, 0, iops[i]])
                }
                for i in range(len(iops))
            }
        )
        return EuclideanSplitter(coll, 5)

    return _gen


def test_euclidean_splitter_calc_value(euclidean):
    splitter = euclidean([1, 2, 3])
    df = pd.DataFrame.from_records([{"path": dcm.filepath} for dcm in splitter.files])
    out = splitter.calc_value(df)
    assert list(out["value"].values) == [1, 1, 0]


@pytest.mark.parametrize(
    "thresh, result", [(0.01, [0.0, 0.0, 0.0]), (0.2, [0.0, 0.0, 5.0])]
)
def test_euclidean_splitter_decision(euclidean, thresh, result):
    dists = [0, 1, 0]
    splitter = euclidean(dists)
    df = pd.DataFrame.from_records(
        [
            {"path": dcm.filepath, "value": val}
            for dcm, val in zip(splitter.files, dists)
        ]
    )
    out = splitter.decision(df, thresh=thresh)
    assert list(out["decision"]) == result


def test_jensenshannon_splitter_distfn(mocker):
    arr = np.random.rand(100).reshape((10, 10))
    a = MagicMock()
    a.dataset.raw.pixel_array = arr
    b = MagicMock()
    b.dataset.raw.pixel_array = arr
    assert not JensenShannonDICOMSplitter.dist_fn(a, b)


@pytest.mark.parametrize(
    "thresh, result", [(0.01, [0.0, 0.0, 0.0]), (0.1, [0.0, 0.0, 5.0])]
)
def test_jensenshannon_splitter_decision(dcmcoll, thresh, result):
    dists = [0.01, 0.3, 0.01]
    coll = dcmcoll(**{"test1": {}, "test2": {}, "test3": {}})
    splitter = JensenShannonDICOMSplitter(coll, 5)
    df = pd.DataFrame.from_records(
        [
            {"path": dcm.filepath, "value": val}
            for dcm, val in zip(splitter.files, dists)
        ]
    )
    out = splitter.decision(df, thresh=thresh)
    assert list(out["decision"]) == result


@pytest.mark.parametrize("numframes", [0, 1, None])
def test_jensenshannon_splitter_distfn_numframes_None_0_1(dcmcoll, jpeg2000, numframes):
    """Test case where DICOM files passed to JensenShannon splitter
    have NumberOfFrames tag explicitly set to 0, 1, or None."""
    data = {
        "PixelData": encaps.encapsulate([jpeg2000]),
        "Rows": 1,
        "Columns": 1,
        "SamplesPerPixel": 3,
        "BitsStored": 8,
        "BitsAllocated": 8,
        "PhotometricInterpretation": "YBR_FULL_422",
        "PixelRepresentation": 0,
        "NumberOfFrames": numframes,
        "PlanarConfiguration": 0,
    }
    coll = dcmcoll(**{"test1": data, "test2": data})
    coll[0].dataset.file_meta.TransferSyntaxUID = uid.JPEG2000
    coll[1].dataset.file_meta.TransferSyntaxUID = uid.JPEG2000
    assert not JensenShannonDICOMSplitter.dist_fn(coll[0], coll[1])


def test_jensenshannon_splitter_distfn_numframes_missing(dcmcoll, jpeg2000):
    """Test case where DICOM files passed to JensenShannon splitter
    have no NumberOfFrames tag."""
    data = {
        "PixelData": encaps.encapsulate([jpeg2000]),
        "Rows": 1,
        "Columns": 1,
        "SamplesPerPixel": 3,
        "BitsStored": 8,
        "BitsAllocated": 8,
        "PhotometricInterpretation": "YBR_FULL_422",
        "PixelRepresentation": 0,
        "PlanarConfiguration": 0,
    }
    coll = dcmcoll(**{"test1": data, "test2": data})
    coll[0].dataset.file_meta.TransferSyntaxUID = uid.JPEG2000
    coll[1].dataset.file_meta.TransferSyntaxUID = uid.JPEG2000
    assert not JensenShannonDICOMSplitter.dist_fn(coll[0], coll[1])


@pytest.mark.parametrize("cls", [UniqueTagSingleSplitter, UniqueTagMultiSplitter])
@pytest.mark.parametrize("output", [["test1", "test2"]])
@pytest.mark.parametrize("input", [(["test1", "test2"]), (["test2", "test1"])])
def test_unique_init(dcmcoll, input, output, cls):
    coll = dcmcoll(**{"test1": {}})
    splitter = cls(coll, 5, tags=input)
    assert splitter.tags == output


def test_uniquesingle_calc_value(dcmcoll):
    coll = dcmcoll(
        **{
            f"test{i}": {"StudyInstanceUID": str(val)}
            for i, val in enumerate([0, 1, 1])
        }
    )
    splitter = UniqueTagSingleSplitter(coll, 5, tags=["StudyInstanceUID", "PatientID"])
    df = pd.DataFrame.from_records([{"path": dcm.filepath} for dcm in splitter.files])
    out = splitter.calc_value(df)
    assert list(out["StudyInstanceUID"].values) == ["0", "1", "1"]
    assert list(out["PatientID"].values) == ["test"] * 3


def test_uniquesingle_decision(dcmcoll):
    coll = dcmcoll(
        **{
            f"test{i}": {"StudyInstanceUID": str(val)}
            for i, val in enumerate([0, 1, 1])
        }
    )
    splitter = UniqueTagSingleSplitter(coll, 5, tags=["StudyInstanceUID", "PatientID"])
    df = pd.DataFrame.from_records(
        [
            {
                "path": dcm.filepath,
                "StudyInstanceUID": dcm.StudyInstanceUID,
                "PatientID": dcm.PatientID,
            }
            for dcm in splitter.files
        ]
    )
    out = splitter.decision(df)
    assert list(out["decision"].values) == [5.0, 0.0, 0.0]


def test_uniquemulti_calc_value(dcmcoll):
    coll = dcmcoll(
        **{
            f"test{i}": {"StudyInstanceUID": str(val)}
            for i, val in enumerate([0, 1, 2])
        }
    )
    splitter = UniqueTagMultiSplitter(coll, 5, tags=["StudyInstanceUID", "PatientID"])
    df = pd.DataFrame.from_records([{"path": dcm.filepath} for dcm in splitter.files])
    out = splitter.calc_value(df)
    assert list(out["StudyInstanceUID"].values) == ["0", "1", "2"]
    assert list(out["PatientID"].values) == ["test"] * 3


def test_uniquemulti_decision(dcmcoll):
    coll = dcmcoll(
        **{
            f"test{i}": {"StudyInstanceUID": str(val)}
            for i, val in enumerate([0, 1, 2])
        }
    )
    splitter = UniqueTagMultiSplitter(coll, 5, tags=["StudyInstanceUID", "PatientID"])
    df = pd.DataFrame.from_records(
        [
            {
                "path": dcm.filepath,
                "StudyInstanceUID": dcm.StudyInstanceUID,
                "PatientID": dcm.PatientID,
            }
            for dcm in splitter.files
        ]
    )
    out = splitter.decision(df)
    assert len(out) == 3


def test_uniquemulti_decision_nan_val_multi(dcmcoll):
    coll = dcmcoll(
        **{f"test{i}": {"AcquisitionNumber": val} for i, val in enumerate([0, 0, None])}
    )
    splitter = UniqueTagMultiSplitter(coll, 5, tags=["AcquisitionNumber", "PatientID"])
    df = pd.DataFrame.from_records(
        [
            {
                "path": dcm.filepath,
                "PatientID": dcm.PatientID,
                "AcquisitionNumber": dcm.AcquisitionNumber,
            }
            for dcm in splitter.files
        ]
    )
    out = splitter.decision(df)
    assert len(out) == 2


def test_uniquemulti_decision_nan_val_single(dcmcoll):
    coll = dcmcoll(
        **{f"test{i}": {"AcquisitionNumber": val} for i, val in enumerate([0, 0, None])}
    )
    splitter = UniqueTagMultiSplitter(coll, 5, tags=["AcquisitionNumber"])
    df = pd.DataFrame.from_records(
        [
            {
                "path": dcm.filepath,
                "AcquisitionNumber": dcm.AcquisitionNumber,
            }
            for dcm in splitter.files
        ]
    )
    out = splitter.decision(df)
    assert len(out) == 2


@pytest.mark.parametrize(
    "orientations, origins",
    [
        ([], []),
        ([(1, 0, 0, 0, 1, 0)], [(0, 0, 0)]),
        ([(1, 0, 0, 1, 0, 0)], [(0, 0, 0)]),
        ([(1, 0, 0, 0, 1, 0)], [(0, 0, 0), (0, 0, 1)]),
        ([(1, 0, 0, 0, 1, 0), (1, 0, 0, 0, 1, 0)], [(0, 0, 0)]),
    ],
)
def test_split_by_geometry_bad_inputs(orientations, origins):
    coll = MagicMock()

    def get_attr(name):
        if name == "ImageOrientationPatient":
            return orientations
        elif name == "ImagePositionPatient":
            return origins

    coll.bulk_get = get_attr
    arg1, arg2 = split_by_geometry(coll)
    assert arg1 == 1


def test_split_by_geometry_normal_inputs(caplog):
    def split_by_geometry_decorator(orientations, origins, max_split=-1):
        coll = MagicMock()

        def get_attr(name):
            if name == "ImageOrientationPatient":
                return orientations
            elif name == "ImagePositionPatient":
                if isinstance(origins[0], list):
                    return origins
                else:
                    return [list(ipp) for ipp in origins]

        coll.bulk_get = get_attr
        return split_by_geometry(coll, max_split_count=max_split)

    # Perform all the split_by_geometry tests using the decorator
    test_geometry.test_compute_phases_by_orientation(
        caplog, split_by_geometry_decorator
    )
    test_geometry.test_compute_phases_by_location(caplog, split_by_geometry_decorator)


def test_split_by_geometry_nones(dcmcoll):
    # GEAR-6388: If IOP or IPP is missing for one or more DICOM files in
    # the collection, DICOMCollection cannot be split by geometry.
    coll = dcmcoll(**{f"test{i}": {} for i in range(3)})

    phase_count, indexes = split_by_geometry(coll)
    assert not phase_count
    assert not indexes
