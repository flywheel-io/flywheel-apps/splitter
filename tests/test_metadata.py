from unittest.mock import MagicMock

import pytest

from fw_gear_dicom_splitter import __version__, pkg_name
from fw_gear_dicom_splitter.metadata import (
    SeriesName,
    add_contributing_equipment,
    gen_series_uid,
    group_by_to_str,
    populate_qc,
    populate_tags,
    update_localizer_frames,
    update_modified_attributes_sequence,
)


def test_update_modified_attributes_sequence(dcmcoll):
    coll = dcmcoll(**{"test1": {}, "test2": {}, "test3": {}})
    update_modified_attributes_sequence(
        coll, {"SeriesInstanceUID": "20.40.60"}, source="special_source"
    )
    for dcm in coll:
        orig_attrs = dcm.get("OriginalAttributesSequence")
        assert len(orig_attrs) == 1
        assert orig_attrs[0].ModifyingSystem == "fw_gear_dicom_splitter"
        assert orig_attrs[0].SourceOfPreviousValues == "special_source"
        assert orig_attrs[0].AttributeModificationDateTime
        assert len(orig_attrs[0].ModifiedAttributesSequence) == 1
        mod_attr = orig_attrs[0].ModifiedAttributesSequence
        assert mod_attr[0].SeriesInstanceUID == "20.40.60"


def test_add_contributing_equipment(dcmcoll):
    coll = dcmcoll(**{"test1": {}, "test2": {}, "test3": {}})
    add_contributing_equipment(coll)
    for dcm in coll:
        cont_equip = dcm.get("ContributingEquipmentSequence")
        assert len(cont_equip) == 1
        assert cont_equip[0].Manufacturer == "Flywheel"
        assert cont_equip[0].ManufacturerModelName == pkg_name
        assert cont_equip[0].SoftwareVersions == __version__


def test_gen_series_uid(dcmcoll):
    coll = dcmcoll(**{"test1": {}, "test2": {}, "test3": {}})
    assert coll.bulk_get("SeriesInstanceUID") == ["1.2", "1.2", "1.2"]

    gen_series_uid(coll)
    assert all(
        [
            uid.startswith("2.16.840.1.114570.4")
            for uid in coll.bulk_get("SeriesInstanceUID")
        ]
    )


@pytest.mark.parametrize(
    "dcmargs, exp",
    [
        ({"test1": {}, "test2": {}, "test3": {}}, "series-1_MR"),
        (
            {
                "test1": {"Modality": "PT"},
                "test2": {"Modality": "PT"},
                "test3": {"Modality": None},
            },
            "series-1_PT",
        ),
        (
            {
                "test1": {"SeriesDescription": "test"},
                "test2": {"SeriesDescription": "test"},
                "test3": {},
            },
            "series-1_MR_test",
        ),
        (
            {"test1": {"SeriesDescription": "test"}, "test2": {}, "test3": {}},
            "series-1_MR",
        ),
        (
            {
                "test1": {"SeriesDescription": "test", "Modality": "PT"},
                "test2": {"SeriesDescription": "test", "Modality": "PT"},
                "test3": {},
            },
            "series-1_PT_test",
        ),
        (
            {
                "test1": {"SeriesDescription": "test/", "Modality": "MR"},
                "test2": {"SeriesDescription": "test/", "Modality": "MR"},
                "test3": {},
            },
            "series-1_MR_test_",
        ),
    ],
)
def test_gen_name(dcmcoll, dcmargs, exp):
    coll = dcmcoll(**dcmargs)
    name = SeriesName.gen_name(coll)

    assert str(name) == exp


def test_update_localizer_frames(dcmcoll):
    coll = dcmcoll(**{"test1": {}, "test2": {}, "test3": {}})
    new_num = update_localizer_frames(coll, "1.2.3", "3")
    for dcm in coll:
        orig_attrs = dcm.get("OriginalAttributesSequence")
        assert len(orig_attrs) == 1
        assert orig_attrs[0].ModifyingSystem == "fw_gear_dicom_splitter"
        assert orig_attrs[0].AttributeModificationDateTime
        assert len(orig_attrs[0].ModifiedAttributesSequence) == 1
        mod_attr = orig_attrs[0].ModifiedAttributesSequence
        assert mod_attr[0].SeriesInstanceUID == "1.2.3"
        assert int(mod_attr[0].SeriesNumber) == 3
        assert int(dcm.SeriesNumber) == 1003
        assert dcm.SeriesInstanceUID != "1.2.3"

    assert int(new_num) == 1003


@pytest.mark.parametrize(
    "split, success",
    [
        (True, True),
        (False, True),
        (False, False),
    ],
)
def test_populate_qc(split, success):
    context = MagicMock()
    context.get_input.return_value = {
        "hierarchy": {"type": "acquisition", "id": "test"},
        "location": {"name": "test"},
        "object": {"modality": None},
    }
    acq = MagicMock()
    file_ = MagicMock()
    file_.name = "my_acq"
    file_.file_id = "test"
    acq.get_file.return_value = file_
    context.client.get_acquisition.return_value = acq

    populate_qc(context, "test", split, success)
    context.metadata.add_qc_result.assert_called_once_with(
        "test",
        "split",
        state=("PASS" if success else "FAIL"),
        data=({"original": {"filename": "my_acq", "file_id": "test"}} if split else {}),
    )


@pytest.mark.parametrize(
    "set_deleted, tags",
    [
        ("deleted", [None, ["splitter"], ["splitter"]]),
        ("retained", [["splitter", "delete"], ["splitter"], ["splitter"]]),
        (None, [["splitter"]]),
    ],
)
def test_populate_tags(set_deleted, tags):
    context = MagicMock()
    context.get_input.return_value = {
        "hierarchy": {"type": "acquisition", "id": "test"},
        "location": {"name": "test"},
        "object": {"modality": None, "tags": ["Yo"]},
    }
    context.get_input_filename.return_value = "test"
    context.config = {"tag": "splitter"}
    file_names = ["test", "file0", "file1"]
    populate_tags(
        context, ["/output/path/file0", "/output/path/file1"], set_deleted=set_deleted
    )
    calls = context.metadata.add_file_tags.call_args_list

    # If set_deleted is "retained" or None, input file exists.
    if set_deleted != "deleted":
        assert calls[0].args[0]["location"]["name"] == file_names[0]
        assert calls[0].args[1] == tags[0]

    # If set_deleted is not None, output files exist.
    if set_deleted == "retained":
        for i in range(1, 3):
            mock_call = calls[i]
            assert mock_call.args[0] == file_names[i]
            assert mock_call.args[1] == tags[i]
    elif set_deleted == "deleted":
        for i in range(len(calls)):
            mock_call = calls[i]
            assert mock_call.args[0] == file_names[1:][i]
            assert mock_call.args[1] == tags[1:][i]


def test_group_by_to_str(dcmcoll):
    tags = [
        [{"FrameReferenceTime": "30000.0"}, "FrameReferenceTime-30000.0"],
        [
            {"FrameReferenceTime": "30000.0", "PatientID": "test"},
            [
                "FrameReferenceTime-30000.0_PatientID-test",
                "PatientID-test_FrameReferenceTime-30000.0",
            ],
        ],
        [
            {
                "FrameReferenceTime": "30000.0",
                "PatientID": "test",
                "SeriesInstanceUID": "1.2",
            },
            [
                "FrameReferenceTime-30000.0_PatientID-test",
                "PatientID-test_FrameReferenceTime-30000.0",
            ],
        ],
        [{"SeriesInstanceUID": "1.2"}, ""],
    ]

    for i, tag in enumerate(tags):
        coll = dcmcoll(**{f"test{i}": tag[0]})
        assert group_by_to_str(coll, list(tag[0].keys())) in tag[1]
