from pathlib import Path
from unittest.mock import MagicMock

import pytest
from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_dicom_splitter.parser import parse_config


@pytest.mark.parametrize(
    "group_by,exp, max_geom_splits",
    [
        ("test1,test2", ["test1", "test2"], -1),
        ("", None, 4),
    ],
)
def test_parse_config(group_by, exp, max_geom_splits):
    gc = MagicMock(spec=dir(GearToolkitContext))

    gc.get_input_path.return_value = "test"
    gc.config = {
        "extract_localizer": True,
        "group_by": group_by,
        "max_geometric_splits": max_geom_splits,
        "zip-single-dicom": "match",
        "delete_input": True,
        "filter_archive": True,
    }

    gear_args = parse_config(gc)

    assert gear_args.dicom_path == Path("test")
    assert gear_args.extract_localizer
    assert gear_args.group_by == exp
    assert gear_args.max_geom_splits == max_geom_splits
    assert gear_args.zip_single
    assert gear_args.delete_input
    assert gear_args.filter_archive
